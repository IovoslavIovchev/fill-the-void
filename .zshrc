HISTFILE=~/.zhist
HISTSIZE=10000
SAVEHIST=100000

# == PATH ==
export PATH="~/bin:$PATH"
export PATH=$PATH:~/bin:/usr/local/go/bin:~/.cargo/bin:~/.local/bin
export GOPATH=~/go
export PATH=$PATH:$GOPATH/bin:~/bin/firefox:~/bin

# prompt stuff
autoload -Uz vcs_info
precmd() { vcs_info }
zstyle ':vcs_info:git:*' formats '[%b]'
setopt PROMPT_SUBST

PROMPT='[%~] [kube ctx: $(kubectl config current-context)] ${vcs_info_msg_0_}'$'\n'"λ "
export VISUAL=$(which nvim)
export EDITOR=$VISUAL

setopt EXTENDED_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_BEEP

# aliases
alias ca='cargo'
alias node='node --use-strict'
alias grep='grep --colo'
alias la='ls -alh'
alias ll='ls -l'
alias c='clear'
alias 'g+'='g++ -pedantic-errors -Wall'
alias ':q'='exit'
alias rib='exit'
alias s='sudo -E'
alias md=mkdir
alias v='nvim'
alias r='ranger'

# Git aliases
alias g='git'
alias ga='g add'
alias gb='g branch'
alias gd='g diff'
alias gds='gd --staged'
alias gf='g fetch'
alias gps='g push; g push --tags'
alias gpl='g pull'
alias gcm='g commit'
alias gch='g checkout'
alias gs='g status'
alias gm='g merge'
alias gst='g stash'
alias gl='g log --oneline'
alias gm='g merge'
alias gr='g reset'
alias grh='g reset --hard'

# kubectl aliases
alias k=kubectl
alias kgp='k get pods'
alias kc='k config'
alias kctx='kc current-context'
alias kl='k logs'
alias kds='k describe'
alias krr='k rollout restart'

kexec() {
    kubectl exec -it $1 -- /bin/bash
}

setopt appendhistory extendedglob nomatch notify hist_ignore_all_dups
unsetopt autocd beep
bindkey -v

zstyle :compinstall filename '/home/crane/.zshrc'

autoload -Uz compinit
compinit

# uncomment if you want kube completions
# source <(kubectl completion zsh)

zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*'
