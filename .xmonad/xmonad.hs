import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.EZConfig (additionalKeys)

import System.Exit
import System.IO

import qualified Data.Map as M
import qualified XMonad.StackSet as W

modm = mod4Mask
term = "st"
lock = "i3lock -c 292626 -n"
dmenu = "dmenu_run -nf '#BBBBBB' -nb '#222222' -sb '#9d0006' -sf '#EEEEEE' -fn 'Roboto-10' -p 'Search: '"

myConfig = defaultConfig
  { modMask = modm
  , terminal = term
  , borderWidth = 1
  , focusedBorderColor = "#665c54"
  , normalBorderColor = "#282828"
  }

myKeys =
  [ ((modm, xK_Return), spawn term)
  , ((modm, xK_q), kill)
  , ((modm .|. shiftMask, xK_r), spawn "xmonad --recompile; xmonad --restart")

  , ((modm .|. shiftMask, xK_h), windows W.swapMaster)
  , ((modm, xK_space), spawn dmenu)
  , ((modm .|. shiftMask, xK_space), sendMessage NextLayout)

  , ((modm .|. shiftMask, xK_z), io $ exitWith ExitSuccess)
  , ((mod4Mask, xK_x), spawn lock)
  , ((modm .|. shiftMask, xK_c), spawn $ "suspend && " ++ lock)
  ]

main = do
  xmobar <- spawnPipe "xmobar"
  let customConfig = myConfig
                       { logHook = dynamicLogWithPP xmobarPP
                                     { ppOutput = hPutStrLn xmobar
                                     , ppTitle = xmobarColor "green" "" . shorten 50
                                     }
                       }
  xmonad $ customConfig `additionalKeys` myKeys
