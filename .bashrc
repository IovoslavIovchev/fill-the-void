# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# NPM config
NPM_PACKAGES="${HOME}/.npm-packages"

PATH="$NPM_PACKAGES/bin:$PATH"

# Unset manpath so we can inherit from /etc/manpath via the `manpath` command
unset MANPATH # delete if you already modified MANPATH elsewhere in your config
export MANPATH="$NPM_PACKAGES/share/man:$(manpath)"
# End NPM config

# ==========
# THE ONE AND ONLY
# ==========
export EDITOR=vim
export VISUAL=$EDITOR

# ----------
# General
# ----------

# Set vi keybindings
set -o vi

# ----------
# Variables
# ----------
export PATH=$PATH:~/scripts:~/.vim/bundle/vim-live-latex-preview/bin:~/bin:$GOBIN
export LD_LIBRARY_PATH=/usr/local/lib
export proj=~/projects/
export term=st
export GOPATH=$proj/go
export GOBIN=$GOPATH/bin

# ----------
# Aliases
# ----------
alias node='node --use-strict'
alias la='ls -alhN --group-directories-first'
alias c='clear'
alias 'g+'='g++ -pedantic-errors -Wall'
alias ':q'='exit'
alias rib='exit'
alias s='sudo -E'
alias md=mkdir
alias v='nvim'
alias r='ranger'

# Git aliases
alias g='git'
alias ga='g add'
alias gb='g branch'
alias gd='g diff'
alias gds='gd --staged'
alias gf='g fetch'
alias gps='g push'
alias gpl='g pull'
alias gcm='g commit'
alias gch='g checkout'
alias gs='g status'
alias gm='g merge'
alias gst='g stash'
alias gl='g log --oneline'
alias gm='g merge'
alias gr='g reset'
alias grh='g reset --hard'

# ----------
# Functions
# ----------
parse_git_branch() {
 git branch 2> /dev/null | sed -e ' /^[^*]/d ' -e ' s/* \(.*\)/ [\1]/ '
}

PS1='[${debian_chroot:+($debian_chroot)}\u@\h] [\w]$(parse_git_branch) \n\$ '
