" ========== General ==========

let mapleader=",,"

syntax on
set number
set wrap
set ruler
set tabstop=4

set mouse=a
set wildignorecase

" Toggle relative line nums only when in focus
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave,InsertEnter * set relativenumber
  autocmd BufLeave,FocusLost   * set norelativenumber
augroup END

filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

" Make Tab 2 spaces
autocmd FileType rl,javascript,html,css,haskell,c,cpp,asm setlocal sw=2 ts=2 sts=2

autocmd BufNewFile,BufRead *.lalrpop set syntax=rust
autocmd BufNewFile,BufRead *.graphql set syntax=typescript
autocmd BufNewFile,BufRead *.svelte set syntax=html
autocmd BufNewFile,BufRead *.rl set filetype=rl

" Filetype-specific keybindings
autocmd BufNewFile,BufRead *.rs nmap <F5>  :Cargo check<CR>G
autocmd BufNewFile,BufRead *.rs nmap <F6>  :RustTest<CR>G
autocmd BufNewFile,BufRead *.rs nmap <F7>  :RustTest!<CR>G
autocmd BufNewFile,BufRead *.rs nmap <F8>  :RustFmt<CR>
autocmd BufNewFile,BufRead *.rs nmap <C-B> :Cbuild<CR>G

set nocompatible
set backspace=indent,eol,start

" Render space as a '.' & tab as a '>>'
set list
set listchars=space:.,tab:>>

nnoremap <C-H> :%s//expand("<cwords")/g

set splitright
set splitbelow

set path+=**

set wildmenu
set wildmode=longest:full,full
set wildignore+=**/node_modules/**
" set wildignore+=**/target/**

" ========== Functions ==========
function! ClosingPairMatch(closing)
    let line = getline('.')
    let col  = col('.')

    return strpart(line, col - 1, 1) == a:closing ? "\<Right>" : join([a:closing, a:closing, "\<Left>"], "")
endf

function! ClosingPairBrackets(closing)
    let line = getline('.')
    let col  = col('.')

    return strpart(line, col - 1, 1) == a:closing ? "\<Right>" : a:closing
endf

" ========== Mappings ==========

cmap vs vsplit
cmap vrs vertical resize
cmap rsz resize
cmap tbe tabedit

" Find my special placeholder symbol
inoremap <leader> <Esc>?<*><Enter>"_2h3xi

" Move left/right in insert mode
inoremap <C-H> <Left>
inoremap <C-L> <Right>

nnoremap ;[   :vertical resize -10<CR>
nnoremap ;]   :vertical resize +10<CR>

nnoremap ; :

" Window-switching
map <C-H> <C-W><C-H>
map <C-J> <C-W><C-J>
map <C-K> <C-W><C-K>
map <C-L> <C-W><C-L>

" Tab-switching
nnoremap <Tab> gt
nnoremap <S-Tab> gT

nnoremap <C-F> /

tnoremap <C-Q> <C-\><C-n>

nmap <C-N> :NERDTreeToggle<CR>
nmap <F9> :noh<CR>

nnoremap <C-P> :CtrlP<CR>

nnoremap <C-K><C-T> !ctags -R .

" ========== Snippets ==========

" LaTeX
autocmd FileType tex inoremap ,bg \usepackage[utf8]{inputenc}<Enter>\usepackage[bulgarian]{babel}

" C++
autocmd FileType c,cpp inoremap ,co std::cout << <*>;
autocmd FileType c,cpp inoremap ,coe std::cout << <*> << '\n';
autocmd FileType c,cpp inoremap ,ci std::cin >> <*>;
autocmd FileType c,cpp inoremap ,if if (<*>) {<CR>}
autocmd FileType c,cpp inoremap ,inc #include<<*>>

autocmd FileType cpp nnoremap ,kk :e %:r.cpp<CR>
autocmd FileType cpp nnoremap ,jj :e %:r.hpp<CR>

" HTML
autocmd FileType html inoremap >> ><Esc>bi<<Esc>yi<f>a</<Esc>pa><Esc>F<i
autocmd FileType html inoremap >><CR> ><Esc>bi<<Esc>yi<f>a</<Esc>pa><Esc>F<i<CR><CR><Esc>ki<Tab>

" JavaScript, TypeScript
autocmd FileType javascript,typescript inoremap ,imp import <*> from '<*>';
autocmd FileType javascript,typescript inoremap ,clg console.log(<*>);

" ========== Plugin-specific ==========

" Pathogen
execute pathogen#infect()

" netrw
let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_liststyle = 3

" NERDTree
let NERDTreeShowHidden = 1
let NERDTreeShowLineNumbers = 1
autocmd FileType nerdtree setlocal relativenumber

" ==============================
" And finally.. the colourscheme
set background=dark
colorscheme gruvbox

" keep file history
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif